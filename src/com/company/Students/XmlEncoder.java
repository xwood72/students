package com.company.Students;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;


public class XmlEncoder {

    public void encode(Object object) throws Exception {
        if (object != null) {
            Object o = object;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            DOMImplementation impl;

            builder = factory.newDocumentBuilder();
            impl = builder.getDOMImplementation();
            Document document = impl.createDocument(
                    null,
                    null,
                    null
            );

            Element root = document.createElement("Object");
            root.setAttribute("type", o.getClass().getSimpleName());
            encodeFields(o, document, root);
            encodeMethods(o, document, root);
            document.appendChild(root);
            printDocument(document);

            System.out.println("asd");
            //TODO сюда добавить поля, методы и ...


        }
    }


    private void encodeFields(Object obj, Document document, Element node) {
        LinkedList<Field> listFields = getListFields(obj);
        Element[] elts = new Element[listFields.size()];
        Iterator<Field> iter = listFields.iterator();
        Element curElement;
        int recurseDeep = 0;

        for (int i = 0; iter.hasNext(); i++) {
            Field f = iter.next();
            curElement = document.createElement("field");
            curElement.setAttribute("type", f.getType().getSimpleName());
            curElement.setAttribute("id", f.getName());

            Object val = getFieldValue(f, obj);
//            if (Collection.class.isAssignableFrom(f.getType()))
//            curElement.setAttribute("value", val.toString());

            if (Collection.class.isAssignableFrom(f.getType()) && recurseDeep < 2) {
                recurseDeep++;
                curElement.setAttribute("value", "[]");
                Object[] containedVals = ((Collection) val).toArray();
                for (Object o : containedVals) {
                    Element collectionElement = document.createElement("Object");
                    collectionElement.setAttribute("type", o.getClass().getSimpleName());
                    encodeFields(o, document, collectionElement);
                    curElement.appendChild(collectionElement);
                }
                recurseDeep--;
            } else {
                curElement.setAttribute("value", val.toString());
            }

            //TODO implements encoding for Map.class
//            if (Map.class.isAssignableFrom(f.getType()) && recurseDeep < 2) {
//                recurseDeep++;
//                Object[] containedVals = ((Map) val).entrySet().toArray();
//                for (Object o : containedVals) {
//                    Element collectionElement = document.createElement("Object");
//                    collectionElement.setAttribute("type", o.getClass().getSimpleName());
//                    encodeFields(o, document, collectionElement);
//                    curElement.appendChild(collectionElement);
//                }
//                recurseDeep--;
//            }

            node.appendChild(curElement);
        }


    }


    private Object getFieldValue(Field field, Object object) {
        Object result = null;

        if ((field.getModifiers() == Modifier.PRIVATE ||
                field.getModifiers() == Modifier.PROTECTED ||
                Modifier.isFinal(field.getModifiers()))) {
            try {
                field.setAccessible(true);
                result = field.get(object);
            } catch (IllegalAccessException e) {
                System.out.println("[ERROR] encodeField: could not set access to field");
                e.printStackTrace();
            } finally {
                field.setAccessible(false);
            }
        } else {
            try {
                result = field.get(object);
            } catch (IllegalAccessException e) {
                System.out.println("[ERROR] encodeField: could not set access to field");
                e.printStackTrace();
            }
        }

        return result;
    }


    private LinkedList<Field> getListFields(Object obj) {
        if (obj != null) {
            LinkedList<Field> result = new LinkedList<>();
            result.addAll(Arrays.asList(obj.getClass().getFields()));
            result.addAll(Arrays.asList(obj.getClass().getDeclaredFields()));

            Class<?> tmpClass = obj.getClass();
            while (tmpClass != null) {
                for (Field f : tmpClass.getDeclaredFields()) {
                    if (f.getModifiers() == Modifier.PROTECTED) {
                        result.add(f);
                    }
                }
                tmpClass = tmpClass.getSuperclass();
            }
            return result;
        } else {
            return new LinkedList<>();
        }
    }


    private LinkedList<Method> getListMethods(Object o) {
        Method[] methods = o.getClass().getMethods();
        Method[] decMethods = o.getClass().getDeclaredMethods(); //public, protected, default (package)
        // access, and private methods
        LinkedList<Method> l = new LinkedList<>();
        for (Method m : methods) {
            l.add(m);
        }
        for (Method m : decMethods) {
            if (m.getModifiers() == Modifier.PRIVATE) {
                l.add(m);
            }
        }
        return l;
    }


    private void encodeMethods(Object obj, Document document, Element element) {
        List<Method> methods = getListMethods(obj);
        Iterator<Method> iter = methods.iterator();
        while (iter.hasNext()) {
            Method m = iter.next();
            Element curMeth = document.createElement("method");
            curMeth.setAttribute("id", m.getName());
            curMeth.setAttribute("return", m.getReturnType().getSimpleName());
            Class[] params = m.getParameterTypes();
            int i = 0;
            for (Class p : params) {
                i++;
                String argName = "arg" + i;
                Element arg = document.createElement("arg");
                arg.setAttribute("id", argName);
                arg.setAttribute("type", p.getSimpleName());
                curMeth.appendChild(arg);
            }
            element.appendChild(curMeth);
        }
    }


    private void printDocument(Document document) {
        DOMSource source = new DOMSource(document);
        StreamResult streamResult = new StreamResult(System.out);
        TransformerFactory transFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            try {
                transformer.transform(source, streamResult);
            } catch (TransformerException e) {
                e.printStackTrace();
            }
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
    }


    private void encodeCollection(Field field) {
        boolean isCollection = Collection.class.isAssignableFrom(field.getType());
    }


}
