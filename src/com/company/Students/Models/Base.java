package com.company.Students.Models;

import java.util.Random;

/**
 * Created by root on 08.06.17.
 */
public class Base {

    public static Long generateHash(Long id){
        return id * 23 + 9;
    }

    public static Long generateId(){
        Random r =new Random();
        int n = r.nextInt(100);
        return System.currentTimeMillis() + n;
    }
}
