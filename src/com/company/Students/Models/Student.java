package com.company.Students.Models;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 08.06.17.
 */
public class Student extends Person implements Externalizable {

    private Long groupId;
    private List<Contact> contacts;
    private final Long id = 123L;

    public Student(String name, String surname, String patronymic, Date birthDate,Long groupId) {
        super(name, surname, patronymic, birthDate);
        this.groupId = groupId;
        this.contacts = new ArrayList<>();
    }

    public Student() {

    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Override
    public Long getId() {
        return id;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public int hashCode() {
        long h = generateHash(id);
        return (int) h;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }

        if (!(obj instanceof Student)){
            return false;
        }

        if (this.id != ((Student) obj).id){
            return false;
        }

        return true;
    }

    public void writeToFile(){
        try {
            FileOutputStream out = new FileOutputStream("Student.txt");
            ObjectOutputStream oout = new ObjectOutputStream(out);
            oout.writeObject(this);
            oout.flush();

        }catch (IOException e){
            e.printStackTrace();
        }

    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getSurname() {
        return super.getSurname();
    }

    @Override
    public void setSurname(String surname) {
        super.setSurname(surname);
    }

    @Override
    public String getPatronymic() {
        return super.getPatronymic();
    }

    @Override
    public void setPatronymic(String patronymic) {
        super.setPatronymic(patronymic);
    }

    @Override
    public Date getBirthDate() {
        return super.getBirthDate();
    }

    @Override
    public void setBirthDate(Date birthDate) {
        super.setBirthDate(birthDate);
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    private void Tratata(){

    }
}
