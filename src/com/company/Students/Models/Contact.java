package com.company.Students.Models;

/**
 * Created by root on 08.06.17.
 */
public class Contact {

    private String value;
    private ContactType type;
    private Long id;

    public Contact(String value, ContactType type) {
        this.value = value;
        this.type = type;
        this.id = System.currentTimeMillis();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return (21 + value.hashCode() * 41) + (21 + type.hashCode() * 41);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }

        if (!(obj instanceof Contact)){
            return false;
        }

        if (this.type != ((Contact) obj).type){
            return false;
        }

        if (this.value != ((Contact) obj).getValue()){
            return false;
        }

        return true;
    }

}
