package com.company.Students.Models;

/**
 * Created by root on 08.06.17.
 */
public enum ContactType {

    //так делать плохо, лучше любые справочные данные хранить в бд

    ADDRESS,
    PHONE,
    EMAIL,
    TELEGRAM,
    VK,
    SKYPE,
    FACEBOOK,
    LINKEDIN,
    ODNOKLASSNIK

}
