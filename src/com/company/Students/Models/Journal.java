package com.company.Students.Models;

import java.util.List;

/**
 * Created by root on 08.06.17.
 */
public class Journal {
    //решить, привязывать журнал к лектору, группе или предмету
    //лучше к группе
    private Integer id;
    private Lesson lesson;
    private Student student;
    private Boolean presence;
    private Long groupId;
    private List<Student> students;


    public Journal() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Journal journal = (Journal) o;

        if (lesson != null ? !lesson.equals(journal.lesson) : journal.lesson != null) return false;
        if (student != null ? !student.equals(journal.student) : journal.student != null) return false;
        return presence != null ? presence.equals(journal.presence) : journal.presence == null;
    }

    @Override
    public int hashCode() {
        int result = lesson != null ? lesson.hashCode() : 0;
        result = 33 * result + (student != null ? student.hashCode() : 0);
        result = 33 * result + (presence != null ? presence.hashCode() : 0);
        return result;
    }
}
