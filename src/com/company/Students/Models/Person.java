package com.company.Students.Models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by root on 08.06.17.
 */
public class Person extends Base implements Serializable {

    protected String name;
    protected String surname;
    protected String patronymic;
    protected Date birthDate;
    protected Long id;
    private int test;

    public Person(String name, String surname, String patronymic, Date birthDate) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
        this.id = generateId();
    }

    public Person() {
        this.id = generateId();
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected String getSurname() {
        return surname;
    }

    protected void setSurname(String surname) {
        this.surname = surname;
    }

    protected String getPatronymic() {
        return patronymic;
    }

    protected void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    protected Date getBirthDate() {
        return birthDate;
    }

    protected void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
