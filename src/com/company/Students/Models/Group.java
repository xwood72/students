package com.company.Students.Models;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by root on 08.06.17.
 */
public class Group implements Externalizable{

    private String caption;
    private List<Student> students;
    private Long id;
    private Journal journal;
    private int groupNum;

    public Group(String caption, List<Student> students, int groupNum) {
        this.caption = caption;
        this.students = new ArrayList<>();
        this.journal = new Journal();
        this.students = students;
        this.groupNum = groupNum;

        Random rnd = new Random();
        int n = rnd.nextInt(100);
        this.id = System.currentTimeMillis() + n;

    }

    public Group() {
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Journal getJournal() {
        return journal;
    }

    public int getGroupNum() {
        return groupNum;
    }

    public void setGroupNum(int groupNum) {
        this.groupNum = groupNum;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    @Override
    public int hashCode() {
        return (int) (19 * id + 31);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }

        if (!(obj instanceof Group)){
            return false;
        }

        if (!this.caption.equals(((Group) obj).getCaption()) ){
            return false;
        }

        if (this.students != ((Group) obj).getStudents()){
            return false;
        }

        return true;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.students);
        out.writeLong(students.size());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        ArrayList<Student> sts = (ArrayList<Student>) in.readObject();
        Long size = in.readLong();
        System.out.println("size: " + size);
    }
}
