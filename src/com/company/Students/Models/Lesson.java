package com.company.Students.Models;

import java.util.Date;
import java.util.List;

/**
 * Created by root on 08.06.17.
 */
public class Lesson {

    private String caption;
    private Date till;
    private Date down;
    private String auditory;
    private String description;
    private String subject;
    private String lector;
    private List<Group> groups;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLector() {
        return lector;
    }

    public void setLector(String lector) {
        this.lector = lector;
    }

    public Date getTill() {
        return till;
    }

    public void setTill(Date till) {
        this.till = till;
    }

    public Date getDown() {
        return down;
    }

    public void setDown(Date down) {
        this.down = down;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getAuditory() {
        return auditory;
    }

    public void setAuditory(String auditory) {
        this.auditory = auditory;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }

        if (!(obj instanceof Lesson)){
            return false;
        }



        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Lesson lesson = (Lesson) obj;

        if (caption != null ? !caption.equals(lesson.caption) : lesson.caption != null) return false;
        if (till != null ? !till.equals(lesson.till) : lesson.till != null) return false;
        if (down != null ? !down.equals(lesson.down) : lesson.down != null) return false;
        if (auditory != null ? !auditory.equals(lesson.auditory) : lesson.auditory != null) return false;
        if (description != null ? !description.equals(lesson.description) : lesson.description != null) return false;
        if (subject != null ? !subject.equals(lesson.subject) : lesson.subject != null) return false;
        if (lector != null ? !lector.equals(lesson.lector) : lesson.lector != null) return false;
        return groups != null ? groups.equals(lesson.groups) : lesson.groups == null;
    }

    @Override
    public int hashCode() {
        int result = caption != null ? caption.hashCode() : 0;
        result = 31 * result + (till != null ? till.hashCode() : 0);
        result = 31 * result + (down != null ? down.hashCode() : 0);
        result = 31 * result + (auditory != null ? auditory.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (lector != null ? lector.hashCode() : 0);
        result = 31 * result + (groups != null ? groups.hashCode() : 0);
        return result;
    }
}
