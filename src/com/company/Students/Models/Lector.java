package com.company.Students.Models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 08.06.17.
 */
public class Lector extends Person {

    private List<Contact> contacts;

    public Lector(String name, String surname, String patronymic, Date birthDate) {
        super(name, surname, patronymic, birthDate);
        this.contacts = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }

        if (!(obj instanceof Lector)){
            return false;
        }


        if (this.id != ((Lector) obj).getId()){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        long h = generateHash(id);
        return (int) h;
    }
}
