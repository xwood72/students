package com.company.Students.Managers;

import com.company.Students.Interfaces.CrudInterface;
import com.company.Students.Models.Group;
import com.company.Students.Models.Student;

import java.io.Serializable;
import java.util.List;

/**
 * Created by root on 08.06.17.
 */
public class ManagerStudents implements CrudInterface, Serializable {

    /**
     * Created by elisium on 08/06/2017.
     */
    private final static long SerialVersionUUID = 1;
    private int amount = 0;
    /*TODO изменить на мап**/
    private List<Group> groups;

    public ManagerStudents(List<Group> groups) {
        this.groups = groups;
    }

    public ManagerStudents() {
    }

    public int getAmountOfStudnts() {
        int sum = 0;
        for (Group g : groups
                ) {
            sum += g.getStudents().size();
        }
        return sum;
    }

    public boolean addStudent(Student student, int groupNum) {
        Long groupId = getGroupId(groupNum);
        if (groupId == -1) return false;
        for (Group group : groups) {
            if (group.getId().equals(groupId)) {
                addStudent(student, group);
                return true;
            }
        }
        return false;
    }

    public Student getStudent(String firstName, String surname, String secondName) throws NullPointerException {
        for (Group group : groups) {
            for (Student student : group.getStudents()) {
                if (student.getName().equals(firstName) &&
                        student.getPatronymic().equals(secondName) &&
                        student.getSurname().equals(surname)) {
                    return student;
                }
            }
        }
        return null;
    }


    public boolean removeStudent(Student student) {
        for (Group group : groups) {
            if (group.getId().equals(student.getGroupId())) {
                removeStudent(student, group);
                return true;
            }
        }
        return false;
    }
//    public Group getGroup(Long id){
//        return groups.get()
//    }

    private Long getGroupId(int groupNum) {
        for (Group group : groups) {
            if (group.getGroupNum() == groupNum) return group.getId();
        }
        return (long) -1;
    }


    private void addStudent(Student student, Group group) {
        student.setGroupId(group.getId());
        group.getStudents().add(student);
    }

    private void removeStudent(Student student, Group group) {
        group.getStudents().remove(student);
    }

    @Override
    public String toString() {
        return groups.toString();
    }

//    @Override
//    public void writeExternal(ObjectOutput out) throws IOException {
//        out.writeObject(groups);
//        out.write(getAmountOfStudnts());
//    }
//
//    @Override
//    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
//        groups = (ArrayList) in.readObject();
//        amount = in.readInt();
//    }


    @Override
    public Object add(Object obj) {
        return null;
    }
}

