package com.company.Students;

import com.company.Students.Models.Contact;
import com.company.Students.Models.ContactType;
import com.company.Students.Models.Student;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by root on 12.06.17.
 */
public class TestXmlEncoding {

    public static void main(String[] args) {
        //Student student = new Student("John", "Connor", "Reese", new Date(), 0l);
//        for (Field field: student.getClass().getDeclaredFields()) {
//            System.out.printf(field.getName() + field.getType().toString());
//        }
//
//        for (Method m: student.getClass().getMethods()) {
//            System.out.println(m.getName() + m.getReturnType().toString() +
//                m.getParameterTypes().length);
//        }
//
//        for (Annotation a: student.getClass().getAnnotations()) {
//            System.out.println(a.annotationType() + a.toString());
//        }
//
//        //сделаем, чтобы приватное поле класса стало доступным
//        try {
//            Field groupId = student.getClass().getDeclaredField("groupId");
////            student.getClass().getField("groupId").setAccessible(true);
//            groupId.setAccessible(true);
//            System.out.println();
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            Field studId = student.getClass().getDeclaredField("id");
//            studId.setAccessible(true);
//            try {
//                Field modifiersField = Field.class.getDeclaredField("modifiers");
//                //studId.setInt(studId,studId.getModifiers() & ~ Modifier.FINAL);
//                studId.setInt(modifiersField ,studId.getModifiers() & ~ Modifier.FINAL);
//                studId.set(student, 1l);
//            } catch (IllegalAccessException e) {
//                e.printStackTrace();
//            }
//            System.out.println(studId.get(student));
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }

        ArrayList<Contact> contacts = new ArrayList<>();
        Contact tel = new Contact("666-666", ContactType.PHONE);
        Contact telegram = new Contact("@test", ContactType.TELEGRAM);
        Contact skype = new Contact("Vasya72", ContactType.SKYPE);
        contacts.add(tel);
        contacts.add(telegram);
        contacts.add(skype);

        Student student = new Student("John", "Connor", "Reese",
                new Date(), 1L);
        student.setContacts(contacts);

        XmlEncoder encoder = new XmlEncoder();
        try {
            encoder.encode(student);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
