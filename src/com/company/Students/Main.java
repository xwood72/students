package com.company.Students;

import com.company.Students.Models.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Date d = new Date(213123);
        Student s = new Student("Victor", "Dorofeev", "Serg", d, 1L);
        Student s1 = new Student("Eugen", "Larg", "Pss", d, 2L);

        s.writeToFile();
        s1.writeToFile();

        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Student.txt"));
            try {
                System.out.println("" + (Student) ois.readObject());
            }catch (ClassNotFoundException e1){
                e1.printStackTrace();
            }
        }catch (IOException e){
            e.printStackTrace();
        }

        ArrayList<Student> students = new ArrayList<>();
        students.add(s);
        students.add(s1);
        Journal j = new Journal();
        Group gr = new Group("STC-06Э", students, 5);
        gr.setStudents(students);


        try {
            FileOutputStream fos = new FileOutputStream("Journal.txt");
            try {
                ObjectOutputStream ous = new ObjectOutputStream(fos);
                //gr.writeExternal(ous);
                ous.writeObject(gr);
                ous.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            FileInputStream fis = new FileInputStream("Journal.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            //ArrayList<Student> st = (ArrayList<Student>) ois.readObject();
           //Long size = (Long) ois.readObject();
            //ArrayList<Student> st = (ArrayList<Student>) ois.readObject();
            Group g = (Group) ois.readObject();
            //System.out.println(st.size());
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


}
